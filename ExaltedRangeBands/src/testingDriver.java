import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.graphstream.ui.spriteManager.Sprite;
import org.graphstream.ui.spriteManager.SpriteManager;

public class testingDriver {
	public static void main(String[] args) {
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");

		ExaltGraph graph = new ExaltGraph("MyGraph");
		
		graph.addNode("Joesus");
		graph.addNode("Moesus");
		graph.addNode("Seigward");
		graph.addNode("Anders");
		graph.addNode("Grimmald");
		graph.addNode("Leo");
		graph.ConnectAllNodes();
		for(Node n: graph){
			if(n.getId()!="Leo") n.addAttribute("ui.class", "solar");
			n.addAttribute("ui.label", n.getId());
		}
		graph.addAttribute("ui.stylesheet", "url(nodes.css)");
		graph.MoveAway("Joesus", "Leo");
		graph.MoveAway("Joesus", "Leo");
		
		
		graph.display();	
		for(Edge e: graph.getEdgeSet()) {
			System.out.println(e.getId() + ": "+ e.getAttribute("layout.weight"));
		}
		System.out.println();
		
		graph.MoveTowards("Joesus", "Moesus");
		for(Edge e: graph.getEdgeSet()) {
			System.out.println(e.getId() + ": "+ e.getAttribute("layout.weight"));
		}
		System.out.println();
		graph.MoveAway("Anders", "Grimmald");
		for(Edge e: graph.getEdgeSet()) {
			System.out.println(e.getId() + ": "+ e.getAttribute("layout.weight"));
		}
		System.out.println();
		graph.MoveAway("Leo", "Joesus");
		for(Edge e: graph.getEdgeSet()) {
			System.out.println(e.getId() + ": "+ e.getAttribute("layout.weight"));
		}
		System.out.println();
		graph.MoveTowards("Anders", "Joesus");
		for(Edge e: graph.getEdgeSet()) {
			System.out.println(e.getId() + ": "+ e.getAttribute("layout.weight"));
		}
	}
	
}
