
import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

public class ExaltGraph extends SingleGraph{
	private final int CLOSE = 1;
	protected ExaltGraph(String id) {
		super(id);
	}
	
	public void MoveTowards(String aID, String bID){
		int oWeight = getEdgeWeight(aID, bID);
		int nWeight = oWeight/2;
		
		UpdateEdge(getEdge(aID, bID), nWeight);
		for(Node n : this.getNodeSet()){
			String nID = n.getId();
			if(nID != aID && nID!=bID){
				if(getEdgeWeight(aID, bID) < getEdgeWeight(bID, nID)){
					UpdateEdge(getEdge(aID, nID), getEdgeWeight(aID, nID)*2);
				}
				else if(getEdgeWeight(aID, bID) > getEdgeWeight(bID, nID)){
					UpdateEdge(getEdge(aID,nID), getEdgeWeight(aID, nID)/2);
				}
			}
		}
	}
	
	public void MoveAway(String aID, String bID){
		int oWeight = getEdgeWeight(aID, bID);
		int nWeight = oWeight* 2;
		
		UpdateEdge(getEdge(aID, bID), nWeight);
		for(Node n : this.getNodeSet()){
			String nID = n.getId();
			if(nID != aID && nID!=bID){
				if(getEdgeWeight(aID, bID) > getEdgeWeight(bID, nID)){
					UpdateEdge(getEdge(aID, nID), getEdgeWeight(aID, nID)*2);
				}
				else if(getEdgeWeight(aID, bID) < getEdgeWeight(bID, nID)){
					UpdateEdge(getEdge(aID,nID), getEdgeWeight(aID, nID)/2);
				}
			}
		}
	}
	
	
	public void UpdateEdge(String aID, String bID, int x){
		this.getEdge(getEdge(aID, bID)).addAttribute("layout.weight", x);;
	}
	
	public void UpdateEdge(String eID, int x){
		this.getEdge(eID).addAttribute("layout.weight", x);
	}
	
	public String getEdge(String aID, String bID){
		for(Edge e : getEachEdge()) {
			if(e.getNode0().getId() == aID && e.getNode1().getId() == bID) return e.getId();
			if(e.getNode0().getId() == bID && e.getNode1().getId() == aID) return e.getId();
		}
		return null;
	}
	
	public int getEdgeWeight(String eID){
		return this.getEdge(eID).getAttribute("layout.weight");
	}
	
	public int getEdgeWeight(String aID, String bID){
		return getEdgeWeight(getEdge(aID, bID));
	}
	
	public void ConnectAllNodes(){
		for(int i = 0; i<this.getNodeCount(); i++){
			String aID = this.getNode(i).getId();
			for(int j = i+1; j<this.getNodeCount(); j++){
				String bID = this.getNode(j).getId();
				this.addEdge(aID+bID, aID, bID);
				UpdateEdge(aID+bID, 1);
			}
		}
	}
}
