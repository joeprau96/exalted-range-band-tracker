import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;

public class driver{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1821816590309083920L;

	public driver() {
		Graph graph = new SingleGraph("myGraph");
		graph.addNode("A");
		graph.addNode("B");
		graph.addNode("C");
		graph.addEdge("AB", "A", "B");
		graph.addEdge("AC","A","C");
		graph.addEdge("BC", "B", "C");
		graph.getEdge("AB").addAttribute("layout.weight", 2);
		graph.display();
	}
}
